# e3-qeproasyn

Wrapper for the module qeproasyn, containing EPICS support for Ocean Optics QEPro spectrometer.

<!-- This README.md should be updated as part of creation and should add complementary information about the wrapped module in question (usage, etc.). Once the repository is set up, empty/unused directories should also be purged. -->

## Requirements

<!-- Put requirements here, like:
- libusb
- ...
-->
libusb

## EPICS dependencies

<!-- Run `make dep` and put the results here, like:
```sh
$ make dep
require examplemodule,1.0.0
< configured ...
COMMON_DEP_VERSION = 1.0.0
> generated ...
common 1.0.0
```
-->

ASYN_DEP_VERSION = 4.42.0+0
SEABREEZE_DEP_VERSION = master
require qeproasyn,master
< configured ...

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

Update USB-rules (see qeproasyn module for more information).

## Usage

```sh
$ iocsh -r "qeproasyn"
```

## Additional information

<!-- Put design info or links (where the real pages could be in e.g. `docs/design.md`, `docs/usage.md`) to design info here.
-->

## Contributing

Contributions through pull/merge requests only.
