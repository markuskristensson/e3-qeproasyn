# Copyright (C) 2022  European Spallation Source ERIC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# The following lines are required
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
include $(E3_REQUIRE_CONFIG)/DECOUPLE_FLAGS

# Most modules only need to be built for x86_64
EXCLUDE_ARCHS = eldk
ARCH_FILTER += linux-x86_64

USR_LIBS += usb

# Module dependencies, with $(ASYN_DEP_VERSION)
# defined in `configure/CONFIG_MODULE
ifneq ($(strip $(ASYN_DEP_VERSION)),)
	asyn_VERSION=$(ASYN_DEP_VERSION)
endif

ifneq ($(strip $(SEABREEZE_DEP_VERSION)),)
	seabreeze_VERSION=$(SEABREEZE_DEP_VERSION)
endif

# Since this file (qeproasyn.Makefile) is copied into the module
# directory at build-time, these paths have to be relative to that path
APP := .
APPDB := $(APP)/Db
APPINC := $(APP)/src
APPSRC := $(APP)/src

USR_INCLUDES += -I ../$(APPINC)
HEADERS += $(APPINC)/drvUSBQEPro.h
HEADERS += $(APPINC)/drvUSBQEProOBP.h
HEADERS += $(APPINC)/drvUSBQEProWorker.h
SOURCES += $(APPSRC)/drvUSBQEPro.cpp
SOURCES += $(APPSRC)/drvUSBQEProOBP.cpp
DBDS = $(APPSRC)/drvUSBQEProSupport.dbd

TEMPLATES += $(wildcard $(APPDB)/*.template)

